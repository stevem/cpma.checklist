from DateTime import DateTime
from transaction import commit

MEMBER_EPOCH = DateTime('2000/1/1')

app = app

site = app.cpma_sites.western

membership_tool = site.portal_membership
group_tool = site.portal_groups
reg_tool = site.portal_registration

group = group_tool.getGroupById('Conference')
members = group.getGroupMembers()

new = [
]

for member in members:
    if member.getProperty('last_login_time') == MEMBER_EPOCH:
        # never logged in
        email = member.getId()
        if len(new) and email not in new:
            next
        password = reg_tool.generatePassword().lower()
        print "login id: %s\npassword: %s\n" % (email, password)
        acl_users = membership_tool._findUsersAclHome(email)
        try:
            acl_users._doChangeUser(
                email,
                password,
                member.getRoles(),
                member.getDomains()
            )
        except RuntimeError:
            print 'ERROR: %s\n' % email
    else:
        print member.getProperty('email'), 'LOGGED IN'
commit()
