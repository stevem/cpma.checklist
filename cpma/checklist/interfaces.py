from zope.interface import Interface


class ISpeakerFolder(Interface):
    """ marker for speaker folder """


class ISpeakerCenter(Interface):
    """ marker for speaker center """
