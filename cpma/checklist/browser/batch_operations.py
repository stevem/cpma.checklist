from plone import api
from plone.i18n.normalizer.interfaces import IIDNormalizer
from Products.Five import BrowserView
from zope.component import queryUtility

import csv
import transaction


group_map = {
    "BLUE": "No Group",
    "FED": "Member Group",
    "G": "No Group",
    "L": "Member Group",
    "M": "Member Group",
    "NM": "Member Group",
    "OS": "Member Group",
    "PMA-M": "Member Group",
    "PMA-N": "Member Group",
    "PMA-O": "Member Group",
    "PURPL": "No Group",
    "RE": "Member Group",
    "SP": "Speaker Group",
    "ST": "Member Group",
    "STAFF": "No Group",
}


class ProcessBatchUpload(BrowserView):
    """process member upload."""

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.normalizer = queryUtility(IIDNormalizer)

    def setup_speaker(self, email, fullname):
        portal = api.portal.get()
        speaker_center = portal['speaker-center']
        speaker_id = self.normalizer.normalize(fullname)
        if speaker_id not in speaker_center.objectIds():
            speaker_folder = api.content.create(
                type='speaker_folder',
                title=fullname,
                container=speaker_center
            )
            # Block acquisition of roles to avoid "Speaker" group visibility
            speaker_folder.__ac_local_roles_block__ = True
            speaker_folder.reindexObjectSecurity()
        else:
            speaker_folder = speaker_center[speaker_id]

        permits = api.user.get_permissions(username=email, obj=speaker_folder)
        if not permits['View']:
            api.user.grant_roles(
                username=email,
                roles=[u'Reader'],
                obj=speaker_folder
            )

        speaker_folder.reindexObjectSecurity()

        checklist = speaker_folder.get('checklist-folder')
        if checklist is None:
            checklist = api.content.create(
                type='checklist_folder',
                title=u"Checklist folder",
                container=speaker_folder,
            )
        api.user.grant_roles(
            username=email,
            roles=[u'Contributor', 'Editor'],
            obj=checklist
        )
        checklist.reindexObjectSecurity()

    def process_file(self):
        context = self.context
        request = self.request
        reg_tool = context.portal_registration
        membership_tool = context.portal_membership
        group_tool = context.portal_groups
        source_users = context.acl_users.source_users

        messages = ""

        upload = request.form.get('uploadfile')
        if upload:
            print "File uploaded."

            dialect = csv.Sniffer().sniff(upload.read(1024))
            upload.seek(0)

            no_group = 0
            added = 0
            processed = 0
            reader = csv.reader(upload, dialect)
            for row in reader:
                if reader.line_num == 1:
                    # header line
                    try:
                        email_pos = row.index('EMAIL')
                        fullname_pos = row.index('FULL_NAME')
                        group_pos = row.index('REGISTRANT_CLASS')
                    except ValueError:
                        messages += "\n" + "Error: The file is not as expected."
                        break
                    # messages += "\n" + "Header"
                else:
                    # messages += "\n" + "content"
                    # messages += "\n %s %s %s" % (email_pos, fullname_pos, group_pos)
                    try:
                        email = row[email_pos].lower()
                        fullname = row[fullname_pos]
                        rclass = row[group_pos]
                    except IndexError:
                        messages += "\n" + "Bad line, #%s, too few columns" % \
                            reader.line_num
                        continue
                    if not (email and fullname and rclass and '@' in email):
                        messages += "\n" + \
                            "Bad line, #%s, lacks email, fullname or class" \
                            % reader.line_num
                        continue

                    # messages += "\n%s %s %s" % (email, fullname, rclass)

                    if rclass == 'RESET':
                        # password reset and notify
                        existing_account = api.user.get(username=email)
                        if existing_account is not None and existing_account.getId() == email:
                            pwd = reg_tool.generatePassword().lower()
                            request.form['darauckPed7'] = pwd
                            source_users.updateUserPassword(email, pwd)
                            transaction.commit()
                            try:
                                reg_tool.registeredNotify(email)
                                messages += "\nRESET id: %s; password: %s" % (email, pwd)
                            except ValueError, value:
                                messages += "\n" + "RESET Unable to email %s, USER NOT NOTIFIED: %s" % (email, value)
                        else:
                            messages += '\nRESET user not found %s' % email
                        continue

                    group = group_map.get(rclass)
                    if group is None:
                        messages += "\n" + \
                            "Bad line, #%s, unknown registrant class" % \
                            reader.line_num
                        continue

                    if group == 'No Group':
                        no_group += 1
                        continue

                    pwd = reg_tool.generatePassword().lower()
                    request.form['darauckPed7'] = pwd

                    properties = {
                        'username': email,
                        'fullname': fullname,
                        'email': email,
                    }

                    membership_added = False
                    existing_account = api.user.get(username=email)
                    if existing_account is not None and existing_account.getId() != email:
                        membership_tool.deleteMembers(existing_account.getId())
                    if existing_account is None or existing_account.getId() != email:
                        try:
                            reg_tool.addMember(email, pwd, properties=properties)
                            added += 1
                            messages += "\nid: %s; password: %s" % (email, pwd)
                            membership_added = True
                        except ValueError, value:
                            messages += "\n" + \
                                "Line %s, %s NOT PROCESSED: %s" % \
                                (reader.line_num, email, value)
                            continue

                    group_tool.addPrincipalToGroup(email, "Conference")
                    if group == 'Speaker Group':
                        group_tool.addPrincipalToGroup(email, "Speakers")
                        self.setup_speaker(email, fullname)

                    processed += 1
                    if processed % 10 == 0:
                        transaction.commit()

                    if membership_added:
                        try:
                            reg_tool.registeredNotify(email)
                        except ValueError, value:
                            messages += "\n" + "Unable to email %s, USER NOT ADDED: %s" % (email, value)
                            membership_tool.deleteMembers([email])
                            added -= 1

            messages += "\n\n%s lines read, %s users processed, %s users added." % \
                (reader.line_num, processed, added)

        # ValueError: The login name you selected is already in use or is not valid. Please choose another.
        # ValueError: You must enter an email address.
        # ValueError: The email address did not validate.

        transaction.commit()
        return messages
