from Products.Five import BrowserView
from zope.component import getMultiAdapter


class DownloadFormView(BrowserView):
    """ form with download header """

    def __call__(self):
        form_id = self.context.getId()
        speaker_id = self.context.aq_parent.aq_parent.getId()
        self.request.response.setHeader("Content-Disposition", 'attachment; filename="%s-%s.html"' % (speaker_id, form_id))

        self.request.form['ajax_load'] = True
        defview = getMultiAdapter((self.context, self.request), name='checklist-view')
        return defview()
