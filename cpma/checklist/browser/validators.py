from z3c.form.validator import SimpleFieldValidator
from zope.interface import Invalid


class SpeakerInformationValidator(SimpleFieldValidator):

    def validate(self, value):
        workshop_only = self.request.form.get('form.widgets.workshop_only') is not None
        if not workshop_only:
            if not value:
                raise Invalid("""
                    You must supply an introduction unless you are only speaking in a workshop or the Assistants' program.
                    """)
            elif len(value) < 30:
                raise Invalid("""
                    You must supply a longer introduction (at least 30 characters) unless you are only speaking in a workshop or the Assistants' program.
                    """)


class MustAgreeValidator(SimpleFieldValidator):

    def validate(self, value):
        if not value:
            raise Invalid("You must agree to complete this form. Press 'Cancel' if you're not ready to agree.")
