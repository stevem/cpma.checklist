from cpma.checklist.browser import all_forms
from plone import api
from plone.dexterity.browser.view import DefaultView


class ChecklistView(DefaultView):
    """ view of a form item """

    def mySpeakerFolder(self):
        context = self.context
        while context.portal_type != 'speaker_folder':
            context = context.aq_parent
        return context.absolute_url()

    def userIsBoss(self):
        member_tool = api.portal.get_tool(name='portal_membership')
        return bool(member_tool.checkPermission(
            'Modify portal content',
            api.portal.get()
        ))

    def my_title(self):
        return dict(all_forms)[self.context.getId()]

    def my_principal(self):
        return self.context.aq_parent.aq_parent.title
