from cpma.checklist.browser import all_forms
from cpma.checklist.browser import form_states
from plone import api
from plone.dexterity.utils import createContent
import plone.protect.utils
from Products.Five import BrowserView
from zope.schema.vocabulary import SimpleTerm
from zope.schema.vocabulary import SimpleVocabulary

forms_available = SimpleVocabulary(
    [SimpleTerm(value=value, title=title) for value, title in all_forms]
)


class SpeakerFolderView(BrowserView):
    """ speaker folder """

    def userIsBoss(self):
        member_tool = api.portal.get_tool(name='portal_membership')
        return bool(member_tool.checkPermission(
            'Modify portal content',
            api.portal.get()
        ))

        # current_user = api.user.get_current().getMemberId()
        # return api.user.has_permission('Modify portal content', username=current_user)

    def addTokenToUrl(self, url):
        return plone.protect.utils.addTokenToUrl(url)

    def allForms(self):
        return all_forms

    def myForms(self):
        """ returns list of dicts with title, state, portal_type and url """

        my_forms = []

        # what's applicable to this speaker?
        applicable = self.context.applicable_forms
        if not applicable:
            return my_forms

        # find out what's already created
        my_context = self.context.get('checklist-folder', None)
        if my_context is None:
            for value, title in all_forms:
                if value in applicable:
                    my_forms.append(dict(
                        title=title,
                        state='missing',
                        state_title=form_states['missing'],
                        portal_type=value,
                        url=''
                    ))
            return my_forms

        brains = api.content.find(
            context=my_context,
            depth=1)
        brez = {}
        for brain in brains:
            brez[brain.portal_type] = brain

        for value, title in all_forms:
            if value in applicable:
                brain = brez.get(value)
                if brain is None:
                    my_forms.append(dict(
                        title=title,
                        state='missing',
                        state_title=form_states['missing'],
                        portal_type=value,
                        url=''
                    ))
                else:
                    my_forms.append(dict(
                        title=title,
                        state=brain.review_state,
                        state_title=form_states[brain.review_state],
                        portal_type=value,
                        url=brain.getURL()
                    ))
        return my_forms

    def mySessions(self):
        my_context = self.context
        brains = api.content.find(
            portal_type='session',
            context=my_context,
            sort_on='getObjPositionInParent',
            depth=1)
        return [b.getObject() for b in brains]

    def sessionPresentations(self, context):
        brains = api.content.find(
            portal_type='presentation_upload',
            context=context,
            depth=1)
        return brains

    def sessionMaterials(self, context):
        brains = api.content.find(
            portal_type='simple_file',
            context=context,
            sort_on='getObjPositionInParent',
            depth=1)
        return [b.getObject() for b in brains]

    def mySpeakerFolder(self):
        context = self.context
        while context.portal_type != 'speaker_folder':
            context = context.aq_parent
        return context.absolute_url()

    def initialFolderView(self):
        member_tool = api.portal.get_tool(name='portal_membership')

        # Create a checklist folder if necessary
        if self.context.get('checklist-folder', None) is None:
            new_checklist = createContent('checklist_folder', id='checklist-folder', title=u"Checklist folder")
            self.context['checklist-folder'] = new_checklist

        # make sure speaker folder contents are usable by speaker
        can_view = member_tool.checkPermission(
            'View',
            self.context
        )
        can_edit = member_tool.checkPermission(
            'Modify portal content',
            self.context
        )
        if can_view:
            if not can_edit:
                # Make sure current user can add to and edit checklist and session folders
                current_user = api.user.get_current()
                for obj in self.context.objectValues():
                    if obj.portal_type in ['checklist_folder', 'session']:
                        roles = api.user.get_roles(user=current_user, obj=obj)
                        if u'Contributor' not in roles:
                            api.user.grant_roles(
                                user=current_user,
                                roles=[u'Contributor', 'Editor'],
                                obj=obj
                            )
            # Are we submitting an agreement?
            if 'form.buttons.speakeragreed' in self.request.form and not self.context.agreed:
                self.context.agreed = True

        return ''


class FindSpeakerView(BrowserView):
    """ find speaker folder """

    def __call__(self):
        self.request.response.redirect(self.mySpeakerFolder())

    def mySpeakerFolder(self):
        context = self.context
        while context.portal_type != 'speaker_folder':
            context = context.aq_parent
        return context.absolute_url()
