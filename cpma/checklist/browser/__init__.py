#

# make validators visible for reload
import validators

all_forms = [
    ('general_information', 'General Information'),
    ('hotel_needs_request', 'Hotel Needs Request'),
    ('audio_visual_agreement', 'Audo/Visual Agreement'),
    ('affiliation_and_disclosure', 'Affiliation and Disclosure'),
    # ('presentation_upload', 'Presentation Upload'),
]

form_states = dict(
    missing='Not started',
    private='Unfinished',
    pending='Pending',
    published='Complete',
)
