<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
    xmlns:tal="http://xml.zope.org/namespaces/tal"
    xmlns:metal="http://xml.zope.org/namespaces/metal"
    xmlns:i18n="http://xml.zope.org/namespaces/i18n"
    lang="en"
    metal:use-macro="context/main_template/macros/master"
    i18n:domain="plone">
<body>

<metal:content-core fill-slot="content-core">
<metal:content-core
    define-macro="content-core"
    tal:define="
        my_contents view/myForms;
        my_sessions view/mySessions;
        user_is_boss view/userIsBoss
        "
    >

<div tal:content="view/initialFolderView" />

<h3 tal:condition="not: context/agreed">
    <a id="agreement-link" tal:attributes="href string:${context/absolute_url}/@@speaker-agreement" title="Read speaker agreement">Please click here to read and agree to your speaker agreement and schedule.</a>
</h3>
<h3 tal:condition="context/agreed">
    <a id="agreement-link" tal:attributes="href string:${context/absolute_url}/@@speaker-agreement" title="Read speaker agreement">Review your speaker agreement and schedule.</a>
</h3>

<div class="form-list" tal:condition="my_contents">
    <h2>Your Forms</h2>
    <p>Thank you for agreeing to participate at <em>The 2017 Western Foot and Ankle Conference</em>, aka <em>The Western</em>. Please review and complete all forms shown below.</p>
    <p>Please click the link shown above to review the agreement which includes your speaking schedule. Please contact <em>The Western</em> with any questions: <a href="mailto:jsteed@calpma.org">jsteed@calpma.org</a>.</p>
    <p>Once you begin filling out a form, you may save at any point and return later. <strong>Once you have completed a form, make sure you mark the item "Done."</strong> Items marked "Done" will be reviewed by <em>The Western</em>'s staff.</p>
    <table>
        <tr tal:repeat="row my_contents">
            <th tal:content="row/title" />

            <tal:block tal:condition="python: row['portal_type'] == 'presentation_upload'">
                <td tal:condition="python: row.get('state') == 'missing'"><a class="btn btn-primary" tal:attributes="href string:checklist-folder/++add++${row/portal_type}">Upload presentation</a></td>
                <tal:block tal:condition="python: row.get('state') == 'private'">
                    <td><a class="btn btn-info" tal:attributes="href string:${row/url}/edit">Update presentation</a></td>
                    <td><a class="btn btn-primary" tal:attributes="href python:view.addTokenToUrl(row['url'] + '/content_status_modify?workflow_action=submit')">Mark this item "Done"</a></td>
                </tal:block>
            </tal:block>
            <tal:block tal:condition="python: row['portal_type'] != 'presentation_upload'">
                <td tal:condition="python: row.get('state') == 'missing'"><a class="btn btn-primary" tal:attributes="href string:checklist-folder/++add++${row/portal_type}">Begin filling out form</a></td>
                <tal:block tal:condition="python: row.get('state') == 'private'">
                    <td><a class="btn btn-primary" tal:attributes="href string:${row/url}/edit">Continue filling out form</a></td>
                    <td><a class="btn btn-primary" tal:attributes="href python:view.addTokenToUrl(row['url'] + '/content_status_modify?workflow_action=submit')">Mark this item "Done"</a></td>
                </tal:block>
            </tal:block>

            <tal:block tal:condition="python: row.get('state') == 'pending'">
                <td>Submitted for review</td>
            </tal:block>
            <tal:block tal:condition="python: row.get('state') == 'published'">
                <td>Accepted</td>
            </tal:block>

            <td tal:condition="python: row['portal_type'] != 'presentation_upload' and user_is_boss and row.get('state') != 'missing'">
                <a tal:attributes="href string: ${row/url}/@@form-download">Download</a>
            </td>

        </tr>
    </table>
</div>

<div
    tal:condition="my_sessions" class="session-list">

    <h2>Your Session Materials</h2>

    <ol>
        <li tal:repeat="session_object my_sessions">
            <div><strong>Session:</strong> <span tal:replace="session_object/title" /></div>
<tal:comment tal:condition="nothing">
            <div tal:define="date session_object/date" tal:condition="date"><strong>Date:</strong> <span tal:replace="date" /></div>
            <div><strong>Session time:</strong> <span tal:replace="session_object/time" /></div>
            <div tal:define="room session_object/room" tal:condition="room"><strong>Room:</strong> <span tal:replace="room" /></div>
            <div tal:define="ltitle session_object/talk_title" tal:condition="ltitle"><strong>Lecture title:</strong> <span tal:replace="ltitle" /></div>
            <div tal:define="ltime session_object/lecture_time" tal:condition="ltime"><strong>Lecture time:</strong> <span tal:replace="ltime" /></div>
            <div tal:define="notes session_object/notes" tal:condition="notes">
                <strong>Notes:</strong>
                <div tal:content="session_object/notes" style="white-space: pre-line" />
            </div>
</tal:comment>


            <dl tal:define="materials python: view.sessionMaterials(session_object);
                            presentations python: view.sessionPresentations(session_object)">
                <tal:block tal:condition="presentations">
                    <dt>Update your presentation slide file:</dt>
                    <dd tal:define="presentation python: presentations[0]" style="margin-top:0.5em">
                        <p><a class="btn btn-default" tal:attributes="href string:${presentation/getURL}/edit">Update</a></p>
                    </dd>
                </tal:block>
                <tal:block tal:condition="not: presentations">
                    <dt>Upload your presentation slide file:</dt>
                    <dd style="margin-top:0.5em">
                        <p><a class="btn btn-primary" tal:attributes="href string:${session_object/absolute_url}/++add++presentation_upload">Upload</a></p>
                    </dd>
                    <dd>
                        <p><em>The Western</em> requests that you use the Western Foot and Ankle Conference template for your Power Point presentation. Please <a href="http://www.thewestern.org/image-resources/western-2017-slide-template.pptx/at_download/file">download the template here</a> (968 kB).</p>
                    </dd>
                </tal:block>
                <dt>
                    Uploaded supplementary materials for this session:
                    <div style="font-weight: normal">
                        <em>Optional.</em> You may upload files here for use as eHandouts (attendee downloads) for additional information or in lieu of your PowerPoint presentation.
                    </div>
                </dt>
                <dd>
                    <ul tal:condition="materials">
                        <li tal:repeat="afile materials">
                            <a tal:attributes="href string: ${afile/absolute_url}/view"><span tal:replace="afile/title_or_id" /></a>
                            (<span tal:replace="afile/file_upload/contentType|nothing" />,
                            <span tal:define="size afile/file_upload/getSize;
                    kb python:size/1024" tal:replace="string: ${kb}KB" />)
                        </li>
                    </ul>
                </dd>
                <dd>
                    <p>
                        <a class="btn btn-primary" tal:attributes="href string:${session_object/absolute_url}/++add++simple_file">Upload</a>
                        <span tal:condition="materials">
                            <a class="btn btn-primary" tal:attributes="href string:${session_object/absolute_url}/folder_contents">Manage your uploaded materials</a>
                        </span>
                    </p>
                </dd>
            </dl>
        </li>
    </ol>
</div>


</metal:content-core>
</metal:content-core>

</body>
</html>