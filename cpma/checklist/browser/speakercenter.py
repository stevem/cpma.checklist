from plone import api
from Products.Five import BrowserView
from zope.component import getMultiAdapter


# form_state = dict(
#     private='Started',
#     pending='Submitted for review',
#     published='Done',
#     )


class SpeakerCenterView(BrowserView):
    """ speaker center """

    def userCanEdit(self):
        member_tool = api.portal.get_tool(name='portal_membership')
        return bool(member_tool.checkPermission(
            'Modify portal content',
            self.context
        ))
        # current_user = api.user.get_current().getMemberId()
        # return api.user.has_permission(
        #     'Modify portal content',
        #     username=current_user,
        #     obj=self.context
        # )

    def speakerFolders(self):
        brains = api.content.find(
            portal_type='speaker_folder',
            context=self.context,
            sort_on='sortable_title',
            depth=1)
        rez = []
        for b in brains:
            obj = b.getObject()
            sessions = []
            my_forms = getMultiAdapter((obj, self.request), name='speakerfolder_view').myForms()
            local_roles = obj.get_local_roles()
            readers = [t[0] for t in local_roles if u'Reader' in t[1]]
            speaker = dict(
                title=obj.title,
                url=obj.absolute_url(),
                sessions=sessions,
                my_forms=my_forms,
                my_readers=readers
            )
            rez.append(speaker)

            session_brains = api.content.find(
                portal_type='session',
                context=obj,
                sort_on='sortable_title',
                depth=1)
            for session_brain in session_brains:
                session = session_brain.getObject()
                uploads = api.content.find(
                    portal_type='presentation_upload',
                    context=session,
                    depth=1)
                materials = api.content.find(
                    portal_type='simple_file',
                    context=session,
                    depth=1)
                sessions.append(dict(
                    title=session_brain.Title,
                    url=session.absolute_url(),
                    upload_count=len(uploads),
                    materials_count=len(materials),
                ))

        return rez

    def allForms(self):
        forms = []
        speaker_brains = api.content.find(
            portal_type='speaker_folder',
            context=self.context,
            depth=1)
        for speaker_brain in speaker_brains:
            speaker = speaker_brain.getObject()
            # who's the speaker?
            role_settings = getMultiAdapter((speaker, self.request), name='sharing').role_settings()
            readers = [role for role in role_settings if role['roles']['Reader'] and not role['roles']['Editor']]
            if len(readers) > 0:
                speaker_id = readers[0]['id']
                user = api.user.get(userid=speaker_id)
                email = user.getProperty('email')
            else:
                email = ''
            my_forms = getMultiAdapter((speaker, self.request), name='speakerfolder_view').myForms()
            for form in my_forms:
                form['name'] = speaker.title
                form['email'] = email
                form['speaker_url'] = speaker.absolute_url()
            forms += my_forms
        return forms


class FormsView(BrowserView):
    """ form list in tab format """

    def __call__(self):
        forms = getMultiAdapter((self.context, self.request), name='form-list').allForms()
        rez = 'Speaker\tSpeaker URL\tEmail\tForm\tState\tForm URL\r\n'
        rez = ''
        for form in forms:
            rez += "{name}\t{speaker_url}\t{email}\t{title}\t{state_title}\t{url}\r\n".format(**form)

        self.request.response.setHeader("Content-Type", 'text/comma-separated-values;charset=utf-8')
        self.request.response.setHeader("Content-Disposition", 'attachment; filename="formlist.tsv"')
        return rez[:-2]
