from interfaces import ISpeakerCenter
from interfaces import ISpeakerFolder
from plone.dexterity.content import Container
from plone.dexterity.content import Item
from zope.interface import implementer

@implementer(ISpeakerFolder)
class SpeakerFolder(Container):
    """ convenience class """


@implementer(ISpeakerCenter)
class SpeakerCenter(Container):
    """ convenience class """